﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ScTapArea : MonoBehaviour, IPointerDownHandler
{
   
   
    public void OnPointerDown(PointerEventData eventData)
    {
        ScGameManager.Instance.CollectByTap (eventData.position, transform);
       
    }
}
