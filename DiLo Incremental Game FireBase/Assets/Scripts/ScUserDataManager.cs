﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ScUserDataManager
{
    private const string PROGRESS_KEY = "Progress";

    public static ScUserProgressData Progress;

    public static void Load(){
        if(!PlayerPrefs.HasKey(PROGRESS_KEY)){
            Progress = new ScUserProgressData();
            Save();
        }
        else
        {
            string json = PlayerPrefs.GetString(PROGRESS_KEY);
            Progress = JsonUtility.FromJson<ScUserProgressData>(json);
        }
    }

    public static void Save(){
        string json = JsonUtility.ToJson(Progress);
        PlayerPrefs.SetString(PROGRESS_KEY,json);
    }
    
    public static bool HasResources(int index){
        if(Progress.ResourcesLevels.Count <= index){
            Debug.Log($"Yas {Progress.ResourcesLevels.Count}");
            // Progress.ResourcesLevels.Add(1);
            // Save();
            return false;
        }

        return true;
    }
    
}
