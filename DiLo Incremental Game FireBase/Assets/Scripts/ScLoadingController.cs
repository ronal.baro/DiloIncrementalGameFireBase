﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class ScLoadingController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        ScUserDataManager.Load();
        SceneManager.LoadScene(1);
    }

}
