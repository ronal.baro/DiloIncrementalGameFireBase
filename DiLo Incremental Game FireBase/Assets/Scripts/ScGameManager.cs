using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScGameManager : MonoBehaviour
{
    #region Singleton
    private static ScGameManager _instance = null;
    public static ScGameManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<ScGameManager>();
            }

            return _instance;
        }
    }

    #endregion 

    [Range(0f, 0.1f)] public float AutoCollectPercentage = 0.1f;
    public ResourceConfig[] ResourceConfigs;
    public Sprite[] ResourceSprites;
    public Transform ResourceParent;
    public ScResourceController ResourcePrefab;

    public ScTapText TapTextPrefab;

    public Transform Coinicon;

    public Text GoldInfo;
    public Text AutoCollectInfo;

    private List<ScResourceController> _activeResources = new List<ScResourceController>();
    private List<ScTapText> _tapTextPool = new List<ScTapText>();

    private float _collectSecond;
    //private double _totalGold;



    private void Start()
    {

        // ResourceConfigs = new ResourceConfig[1];

        // ResourceConfigs[0].name = "Click";
        // ResourceConfigs[0].unlockCost= 0;
        // ResourceConfigs[0].upgradeCost=1 ;
        // ResourceConfigs[0].output = 2;
        AddAllResources();

        GoldInfo.text = $"Gold: {ScUserDataManager.Progress.Gold.ToString("0")}";

    }

    private void Update()
    {
        _collectSecond += Time.unscaledDeltaTime;
        if (_collectSecond >= 1f)
        {
            CollectPerSecond();
            _collectSecond = 0f;

        }

        CheckResourceCost();
        Coinicon.transform.localScale = Vector3.LerpUnclamped(Coinicon.transform.localScale, Vector3.one * 2f, 0.15f);
        Coinicon.transform.Rotate(0f, 0f, Time.deltaTime * -100f);

    }

    private void CheckResourceCost()
    {
        foreach (ScResourceController resource in _activeResources)
        {
            bool isBuyable = false;
            if (resource.IsUnlocked)
            {
                isBuyable = ScUserDataManager.Progress.Gold >= resource.GetUpgradeCost();
            }
            else
            {
                isBuyable = ScUserDataManager.Progress.Gold >= resource.GetUnlockCost();
            }
            resource._resourceImage.sprite = ResourceSprites[isBuyable ? 1 : 0];
        }
    }

    public void CollectByTap(Vector3 tapPosition, Transform parent)
    {
        double output = 0;
        foreach (ScResourceController resourceTemp in _activeResources)
        {
            if (resourceTemp.IsUnlocked)
            {
                output += resourceTemp.GetOutput();
            }
        }

        ScTapText tapText = GetOrCreateTapText();
        tapText.transform.SetParent(parent, false);

        tapText.transform.position = tapPosition;
        //Kalau Canvas Render Mode nya camera bukan overlay bakalan kacau...

        //Debug.Log(tapPosition + " " + tapText.transform.position);
        tapText.Text.text = $"+{output.ToString("0")}";
        tapText.gameObject.SetActive(true);
        Coinicon.transform.localScale = Vector3.one * 1.75f;

        AddGold(output);

    }

    private ScTapText GetOrCreateTapText()
    {
        ScTapText tapText = _tapTextPool.Find(t => !t.gameObject.activeSelf);
        // cari yang tidak aktif, kayaknya return yang terakhir... biar gak ngeflood kali andai ditekan berkali2)..
        // jadi kayaknya sih banyak isi dari taptextPool itu sebanyak si player bisa klik dalam kurun waktu si yang aktif
        // bingung kenapa tidak di destroy aja? mungkin buat performa? apa create gameobjek berkali2 cendrung lambat mungkin?
        // jadi lebih baik reuse yang sudah ada?
        // kesimpulan awalnya.. pemain lambat diimbangi dengan kecepatan performa gamenya itu sendiri LMAO.
        //---------------------30sec later
        // yah ketemu https://forum.unity.com/threads/object-pooling-activating-a-game-object-is-so-slow-its-not-that-much-better-than-instantiating.454331/

        if (tapText == null)
        {
            tapText = Instantiate(TapTextPrefab).GetComponent<ScTapText>();
            _tapTextPool.Add(tapText);
        }
        return tapText;
    }

    private void AddAllResources()
    {   
        bool showResources = true;
        int index=0;
        foreach (ResourceConfig configTemp in ResourceConfigs)
        {
            GameObject obj = Instantiate(ResourcePrefab.gameObject, ResourceParent, false);
            ScResourceController resource = obj.GetComponent<ScResourceController>();  //

            resource.SetConfig(index,configTemp);

            obj.gameObject.SetActive(showResources);
            if (showResources && !resource.IsUnlocked)
            {
                showResources = false; //memastikan hanya resource pertama yang locked yg ditampilkan maka setelah dia, yang lain show resource nya false..
            }

            _activeResources.Add(resource);
            index++;
        }
    }

    public void ShowNextResource()
    {
        foreach (ScResourceController resource in _activeResources)
        {
            if (!resource.gameObject.activeSelf)
            {
                resource.gameObject.SetActive(true);
                break;
                //lol kalau aku mungkin aku bakalan catat posisinya aja wkwk; (Ingat pake kayak di atas aja!!!..)
            }
        }
    }

    private void CollectPerSecond()
    {
        double output = 0;
        foreach (ScResourceController resourceTemp in _activeResources)
        {
            if (resourceTemp.IsUnlocked)
            {
                output += resourceTemp.GetOutput();
            }
        }

        output *= AutoCollectPercentage;
        AutoCollectInfo.text = $"Auto Collect : {output.ToString("F1")} / second";

        AddGold(output);
    }

    public void AddGold(double value)
    {
        ScUserDataManager.Progress.Gold += value;
        GoldInfo.text = $"Gold : {ScUserDataManager.Progress.Gold.ToString("0")}";

        ScUserDataManager.Save();
    }

    public double TotalGold
    {
        get { return ScUserDataManager.Progress.Gold; }
    }

}


[System.Serializable]
public struct ResourceConfig
{
    public string name;
    public double unlockCost;
    public double upgradeCost;
    public double output;
}

