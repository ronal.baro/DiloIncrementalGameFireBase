﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class ScUserProgressData
{
    public double Gold = 0;
    public List<int> ResourcesLevels = new List<int>();

}

